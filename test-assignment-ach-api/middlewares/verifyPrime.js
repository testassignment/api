// const models = require("../../models/index");

checkPrime = (req, res, next) => {
  let today = new Date();
  let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  let dateTime = date+' '+time;
  let isPrimesArray = [];
  console.log("end - start : ",req.body.start - req.body.end)
  for(i=0;i<(req.body.end - req.body.start)+1;i++){
    let primeNumber = isPrime(req.body.start+i)
    if(primeNumber == 0 )
    continue;
    isPrimesArray.push(primeNumber)
  }
  // console.log(isPrimes)
  req.primesArray = isPrimesArray;
  next();
};

isPrime=(number)=>{
  if(number<2)
		return 0;
	if(number==2 || number==3)
		return number;
	if(number%2==0 || number%3==0)
		return 0;
	for(j=5; j*j <= number; j+=6){
		if(number%j==0 || number%(j+2)==0)
			return 0;
	}
	return number;
}


const verifyPrime = {
  checkPrime: checkPrime,
};

module.exports = verifyPrime;