const { verifyPrime } = require("../middlewares");

const express = require('express') // เรียกใช้ Express
const router = express.Router();
const submitPrimeController = require('../controllers/submitPrimeController')
const requestPrimeController = require('../controllers/requestPrimeController')
router.post('/submit',[verifyPrime.checkPrime],submitPrimeController.index);
router.post('/request',requestPrimeController.index);

module.exports = router;