console.log("welcome to \"TEST ASSIGNMENT ACH\" Api ");
const path = require('path')
const express = require('express') // เรียกใช้ Express
const bodyParser = require('body-parser');
const cors = require("cors");
// const mysql = require('mysql') // เรียกใช้ mysql

const primeRoutes = require('./routes/prime');

const app = express() // สร้าง Object เก็บไว้ในตัวแปร app เพื่อนำไปใช้งาน
app.use(cors({ origin: "*" }));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(function(req,res,next){
  res.setHeader('Access-Control-Allow-origin','*');
  res.setHeader('Access-Control-Allow-Methods','GET','POST','PUT','DELETE');
  //res.setHeader('Access-Control-Allow-Headers','content-type','x-access-token');
  res.setHeader('Access-Control-Allow-Credentials',true);
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});
// *************************************** API LINK HERE ****************************************************
app.use('/api/prime',primeRoutes)


app.listen('3001',() => {     // 
console.log('start port 3001')  
})