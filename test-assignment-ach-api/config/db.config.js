const mysql = require('mysql') // เรียกใช้ mysql

const db = mysql.createConnection({   // config ค่าการเชื่อมต่อฐานข้อมูล
  
    host     : 'localhost', 
    user     : 'root',
    password : '',
    database : 'assignment_ach' // ชื่อ db
    })
    
  db.connect()

module.exports = db;

  // const config = {
  //   db: {
  //     /* don't expose password or any sensitive info, done only for demo */
  //     host: "localhost",
  //     user: "root",
  //     password: "",
  //     database: "pookpinto",
  //   },
  // };
  // module.exports = config;